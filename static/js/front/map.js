// Вспомогательная функция для создания кастомного управления масштабом
function controls(controlsBox, map) {
    google.maps.event.addDomListener(minus, 'click', function() {
        var currentZoomLevel = map.getZoom();
        if(currentZoomLevel != 0){
            map.setZoom(currentZoomLevel - 1);}
    });

    google.maps.event.addDomListener(plus, 'click', function() {
        var currentZoomLevel = map.getZoom();
        if(currentZoomLevel != 21){
            map.setZoom(currentZoomLevel + 1);}
    });
}
var map;

function initialize() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
        center: new google.maps.LatLng(39.5403, 65.5463),
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        disableDefaultUI: true
        // zoomControl: true,
        // zoomControlOptions: {
            // style: google.maps.ZoomControlStyle.SMALL,
            // position: google.maps.ControlPosition.RIGHT_BOTTOM
        // }
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);



    var waterColor = '#cbdce4';
    var dividerColor = '#91c1d2';

    map.set('styles', [
    {
        featureType: 'administrative',
        elementType: 'geometry.stroke',
        stylers: [
            { color: dividerColor }
        ]
    }, {
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [
            { hue: '#ffff00' },
            { gamma: 1.4 },
            { saturation: 82 },
            { lightness: 96 }
        ]
    }, {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [
            { color: waterColor }
        ]
    }, {
        featureType: 'water',
        elementType: 'labels',
        stylers: [
            { visibility: "off" }
        ]
    }, {
         featureType: "administrative",
         elementType: "geometry.fill",
         stylers: [
            { visibility: "off" }
         ]
       }
    ]);

// Вновь для создания кастомного управления масштабом
    var controlsBox = document.getElementById('controls');
    var myControl = new controls(controlsBox, map);

// Расстановка маркеров
    setMarkers(map, markers);

}

var markers = [
    ['a', 62.859332, 43.453793, 6],
    ['b', 61.631647, 48.727230, 5],
    ['Turkey', 40.620918, 37.916683, 4],
    ['c', 43.872834, 42.486995, 3],
    ['d', 53.780111, 102.604181, 2],
    ['j', 44.877946, 135.123711, 1]
];

function setMarkers(map, locations) {
    // Add markers to the map

    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.

    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.
    var image = {
        url: '/static/img/map/marker-mini.png',
    // This marker is 20 pixels wide by 32 pixels tall.
        size: new google.maps.Size(16, 26)
    // The origin for this image is 0,0.
        // origin: new google.maps.Point(8, 26)
    // The anchor for this image is the base of the flagpole at 0,32.
        // anchor: new google.maps.Point(0, 26)
    };
  // Shapes define the clickable region of the icon.
  // The type defines an HTML &lt;area&gt; element 'poly' which
  // traces out a polygon as a series of X,Y points. The final
  // coordinate closes the poly by connecting to the first
  // coordinate.
  var shape = {
    //   coords: [1, 1, 1, 20, 18, 20, 18 , 1],
    //   type: 'poly'
  };
  for (var i = 0; i < locations.length; i++) {
    var mark = locations[i];
    var myLatLng = new google.maps.LatLng(mark[1], mark[2]);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        shape: shape,
        title: mark[0],
        zIndex: mark[3]
    });
  }
}

google.maps.event.addDomListener(window, 'load', initialize);
