import re
import json

from django import template
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from tools.various.db import date_handler

register = template.Library()


@register.simple_tag
def active(request, pattern):
    """
    Template tag to highlight selected page in the menu
    """
    if re.search(pattern, request.path):
        return 'active'
    return ''


@register.filter()
def jsonify(obj):
    if isinstance(obj, QuerySet):
        return serialize('json', obj)
    return json.dumps(obj, default=date_handler)