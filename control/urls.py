from django.conf.urls import patterns, url

from config.settings import LANGUAGES_TUPLE
from control.api import ContactAjaxView, DirectionsAjaxView, BannerAjaxView, OfficeAjaxView


langs = '|'.join(LANGUAGES_TUPLE)

urlpatterns = patterns('control',
                        url(r'^$', 'views.index', name='control-main'),
                        url(r'^directions/$', 'views.directions', name='directions'),
                        url(r'^contacts/(?P<lang>(' + langs + '))/$', 'views.contacts', name='contacts'), )

# API
urlpatterns += patterns('control',
                        url(r'^api/banners/$', BannerAjaxView.as_view()),
                        url(r'^api/directions/$', DirectionsAjaxView.as_view(), name='api-directions'),
                        url(r'^api/offices/$', OfficeAjaxView.as_view(), name='api-offices'),
                        url(r'^api/contacts/$', ContactAjaxView.as_view(), name='api-contacts'), )
