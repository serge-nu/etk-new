# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.utils import translation
from exampleapp.models import Contact, Directions, Office, Depts


def index(request):
    return render(request, 'control/index.html', {})


def contacts(request, lang='ru'):
    translation.activate(lang)
    contacts = Office.export_control_all()
    # depts = Depts.objects.all()
    print contacts
    return render(request, 'control/contacts.html', {'contacts': contacts, 'lang': lang, })


def directions(request):
    directions = Directions.export_control_all()
    print directions
    return render(request, 'control/directions.html', {'directions': directions, })
