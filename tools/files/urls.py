from django.conf.urls import patterns, url

urlpatterns = patterns('tools.files.views',

	         url(r'^upload_temp_file/$', 'upload_temp_file', name='control-temp-file-upload'),
	         url(r'^upload_temp_file/(\w+)/$', 'upload_temp_file', name='control-temp-file-upload-p'),
	         url(r'^upload_temp_files/$', 'upload_temp_files', name='control-temp-file-uploads'),
	         url(r'^upload_temp_files/(\w+)/$', 'upload_temp_files', name='control-temp-file-uploads-p'),

	         url(r'^detect_color_type/$', 'detect_color_type', name='control-temp-file-detect-color-type'),

)

