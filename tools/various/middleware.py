from django.contrib.sessions.middleware import SessionMiddleware
from django.core.urlresolvers import reverse, resolve
from django.http import HttpResponseRedirect


class Redirect404(object):
    def process_response(self, request, response):
        if response.status_code != 404 or request.path.startswith('/media/') or request.path.startswith(
                '/files/') or request.path.startswith('/static/'):
            return response  # No need to check for a redirect for non-404 responses.
        return HttpResponseRedirect(reverse('front-home'))


class ViewNameMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):
        url_name = resolve(request.path).url_name
        request.url_name = url_name


class CustomSessionMiddleware(SessionMiddleware):
    def process_request(self, request):
        if not request.session.exists(request.session.session_key):
            request.session.create()