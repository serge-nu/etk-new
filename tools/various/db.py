# -*- coding: utf-8 -*-
import re

from django.db import models
from django.db.models import FieldDoesNotExist


FIRST_CAP_RE = re.compile('(.)([A-Z][a-z]+)')
ALL_CAP_RE = re.compile('([a-z0-9])([A-Z])')


# Конвертирует строку написанную в CamelCase (например MyVariable)
# в строку с подчеркивания (например my_variable)
def convert(name):
    s1 = FIRST_CAP_RE.sub(r'\1_\2', name)
    return ALL_CAP_RE.sub(r'\1_\2', s1).lower()


def underscore_to_camelcase(value):
    words = value.split('_')
    return ''.join(word.title() for i, word in enumerate(words))


def create_or_update_and_get(model_class, data):
    instance = get_or_create(model_class, data)
    return update(instance, data)


def get_or_create(model_class, data):
    if model_class._meta.pk.name in data.keys():
        get_or_create_kwargs = {
            model_class._meta.pk.name: data.pop(model_class._meta.pk.name)
        }
    else:
        get_or_create_kwargs = {
            model_class._meta.pk.name: None
        }
    try:
        # get
        instance = model_class.objects.get(**get_or_create_kwargs)
    except model_class.DoesNotExist:
        # create
        instance = model_class(**get_or_create_kwargs)

    return instance


def update(instance, data):
    # update (or finish creating)
    for key, value in data.items():
        try:
            field = type(instance)._meta.get_field(key)
        except FieldDoesNotExist:
            continue
        if not field:
            continue
        if isinstance(field, models.ForeignKey) and hasattr(value, 'items'):
            rel_instance = get_or_create(field.rel.to, value)
            rel_instance = update(rel_instance, value)
            setattr(instance, key, rel_instance)
        elif isinstance(field, models.BooleanField):
            if value is None:
                value = field.get_default()
            setattr(instance, key, value)
        else:
            if not value and field.blank:
                value = field.get_default()
            setattr(instance, key, value)
    instance.save()
    return instance


def date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))
