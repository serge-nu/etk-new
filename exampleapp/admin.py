from django.contrib import admin

# Register your models here.
from .models import Contact
import models

from vendor.multilingual_model.admin import TranslationStackedInline

class ChoiceInline(admin.TabularInline):
    model = Contact
    extra = 2

class ContactAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['title']}),
        ('Date information', {'fields': ['url'], 'classes': ['collapse']}),
    ]
    # inlines = [ChoiceInline]
    # list_display = ('title', 'pub_date', 'was_published_recently')
    # list_filter = ['pub_date']
    # search_fields = ['title']

class PageTranslationInline(TranslationStackedInline):
    model = models.PageTranslation

class PageAdmin(admin.ModelAdmin):
    list_display = ['type', '__unicode__']
    inlines = [PageTranslationInline]

class NewsIndexInline(admin.TabularInline):
    model = models.NewsIndex

class NewsIndexAdmin(admin.ModelAdmin):
    list_display = ['newsTitle', 'newsText', 'newsDate', 'id']

# class ProjectsIndexInline(admin.TabularInline):
#     model = models.ProjectsIndex

# projects on main page
class ProjectsIndexTranslationInline(TranslationStackedInline):
    model = models.ProjectsIndexTranslation

class ProjectsIndexAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'id']
    inlines = [ProjectsIndexTranslationInline]

# banners
class BannersTranslationInline(TranslationStackedInline):
    model = models.BannersTranslation

class BannersAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'id']
    inlines = [BannersTranslationInline]

# partners logos
class PartnersInline(admin.TabularInline):
    model = models.Partners

class PartnersAdmin(admin.ModelAdmin):
    list_display = ['title', 'id']

# projects
class ProjectsTranslationInline(TranslationStackedInline):
    model = models.ProjectsTranslation

class ProjectsAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'id']
    inlines = [ProjectsTranslationInline]

#directions
class DirectionsTranslationInline(TranslationStackedInline):
    model = models.DirectionsTranslation

class DirectionsAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'id']
    inlines = [DirectionsTranslationInline]

#offices
class OfficeTranslationInline(TranslationStackedInline):
    model = models.OfficeTranslation

class OfficeAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'id']
    inlines = [OfficeTranslationInline]

#depts
class DeptsTranslationInline(TranslationStackedInline):
    model = models.DeptsTranslation

class DeptsAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'id']
    inlines = [DeptsTranslationInline]

admin.site.register(Contact, ContactAdmin)
admin.site.register(models.Page, PageAdmin)
admin.site.register(models.NewsIndex, NewsIndexAdmin)
admin.site.register(models.ProjectsIndex, ProjectsIndexAdmin)
admin.site.register(models.Projects, ProjectsAdmin)
admin.site.register(models.Directions, DirectionsAdmin)
admin.site.register(models.Banners, BannersAdmin)
admin.site.register(models.Partners, PartnersAdmin)
admin.site.register(models.Office, OfficeAdmin)
admin.site.register(models.Depts, DeptsAdmin)
