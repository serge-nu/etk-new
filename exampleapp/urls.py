from django.conf.urls import patterns, url

urlpatterns = patterns('exampleapp',
                        url(r'^$', 'views.index', name='index'),
                        url(r'^contacts/$', 'views.contact', name='contact'),
                        url(r'^newsindex/$', 'views.newsindex', name='newsindex'),
                        url(r'^projects/$', 'views.projects', name='projects'),
                        # url(r'^contacts/$', 'views.contacts', name='contacts'),
)
