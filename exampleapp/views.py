# -*- coding: utf-8 -*-

import sys
import traceback

from django.template import loader, Context, RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.http import HttpResponseServerError

from .models import Contact, Page, NewsIndex, ProjectsIndex, Directions, Projects, Banners, Partners, Office, Depts

def index(request):
    newsindex = NewsIndex.objects.all()
    projectindex = ProjectsIndex.export_all()
    banners = Banners.export_all()
    print banners
    return render(request, 'index.html', {'newsindex': newsindex,
                                          'projectindex': projectindex,
                                          'banners': banners,
                                          'partners': partners, })

def projects(request):
    projects = Projects.export_all()
    directions = Directions.export_control_all()
    print projects
    return render(request, 'projects.html', {'projects': projects,
                                             'directions': directions, })

# def contact(request):
#     contact = Contact.objects.get()
#     print contact
#     return render(request, 'news-index.html', {'contact': contact, })

def contact(request):
    offices = Office.objects.all()
    depts = Depts.objects.all()
    return render(request, 'contacts.html', {'offices': offices,
                                               'depts': depts, })

def custom_500(request):
    t = loader.get_template('500.html')

    print sys.exc_info()
    type, value, tb = sys.exc_info()

    tbf = traceback.format_exception(type, value, tb)
    tbfs = ''
    for tbfi in tbf:
        tbfs += '\n' + tbfi
    return HttpResponseServerError(t.render(Context({
        'exception_value': value,
        'value': type,
        'tb': tbfs})))


def custom_404(request):
    return redirect(reverse('exampleapp:contacts'))
