from .models import MENU_TYPES, Page

from django.conf import settings
from django.utils.translation import get_language

from settings import Lang


def my_vars(request):
    if get_language() == Lang.Eng:
        lang_url = ''
    else:
        lang_url = get_language()

    return {'DEBUG': settings.DEBUG,
            'LANG_URL': lang_url,
            'HEADER_MENU': Page.objects.filter(menu=MENU_TYPES[1][0], is_active=True),
            'FOOTER_MENU': Page.objects.filter(menu=MENU_TYPES[2][0], is_active=True), }
