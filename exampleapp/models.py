# -*- coding: utf-8 -*-

# new
import re
import os
from datetime import datetime

from django.db import models
from django.core.files import File
from django.utils import translation, timezone
from tools.files.models import TempFile
# new
from tools.files.fields import CustomImgField

# new
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _, get_language
# from pytils.translit import slugify

from django.utils.translation import gettext as _
from vendor.multilingual_model.models import MultilingualModel, MultilingualTranslation

from tools.various.db import convert, get_or_create, update
from config.settings import Lang
from exampleapp.utils import tuplify, convert, create_or_update_and_get, update, get_or_create, save_translations

SECTION_TYPES = (
    ('main', 'main'),
    ('discovery', 'discovery'),
)

PAGE_TYPES = (
    ('index', _(u'Главная')),
    ('projects', _(u'Проекты')),
    ('projects-inside', _(u'Подпроекты')),
    ('about', _(u'О компании')),
    ('news', _(u'Новости')),
    ('contacts', _(u'Контакты')),
)

MENU_TYPES = (
    ('', _(u'Нет')),
    ('header', _(u'Меню в заголовке')),
    ('footer', _(u'Нижнее меню')),
)


class Base(models.Model):
    """
    Абстрактная модель с часто используемыми методами
    """
    order = models.IntegerField(blank=True, default=0)

    def export_front(self):
        pass

    @classmethod
    def export_front_all(cls, *args, **kwargs):
        items_list = cls.objects.filter(*args, **kwargs)
        return [item.export_front() for item in items_list]

    def export_control(self):
        pass

    @classmethod
    def export_control_all(cls, *args, **kwargs):
        if kwargs.get('lang'):
            lang = kwargs.pop('lang')
            translation.activate(lang)
        items_list = cls.objects.filter(*args, **kwargs)
        return [item.export_control() for item in items_list]

    @classmethod
    def import_all(cls, data, parent=None):
        objs = []
        for item in data:
            objs.append(cls.import_item(item, parent))

        return objs

    @classmethod
    def import_item(cls, data, parent=None):
        pass

    @classmethod
    def simple_import(cls, data):
        converted_data = {}

        for key, value in data.iteritems():
            converted_data[convert(key)] = value

        obj = get_or_create(cls, converted_data)
        if converted_data.get('removed'):
            obj.delete()
            return None, None
        obj = update(obj, converted_data)

        return obj, converted_data

    def import_image(self, data):
        if data.get('tempImgId'):
            temp_image = TempFile.objects.get(id=data.get('tempImgId'))
            getattr(self, data.get('imageType')).save(temp_image.file.name, File(open(temp_image.file.path, 'rb')))

    class Meta:
        ordering = ('order', 'id',)
        abstract = True


class Contact(Base):
    title = models.CharField(max_length=100, blank=True, default='')
    url = models.CharField(max_length=100, blank=True, default='')

    # new
    # question_text = models.CharField(max_length=200)
    # pub_date = models.DateTimeField('date published')
    # def __str__(self):
    # return self.question_text

    @classmethod
    def import_item(cls, data, parent=None):
        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        return model

    def export_front(self):
        return {
            'id': self.id,
            'title': self.title,
            'order': self.order,
            'url': self.url,
        }

    def export_control(self):
        return {
            'id': self.id,
            'title': self.title,
            'order': self.order,
            'url': self.url,
        }

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = _(u'Контакты')


#
#
#
#

class PageTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Page', related_name='translations')
    title = models.CharField(max_length=50, blank=True, default='', verbose_name=_(u'Название страницы'))
    subtitle = models.CharField(max_length=50, blank=True, default='', verbose_name=_(u'Подзаголовок страницы'))
    body = models.TextField(blank=True, default='', verbose_name=_(u'Текст страницы'))

    class Meta:
        unique_together = ('parent', 'language_code')


class Page(MultilingualModel):
    parent = models.ForeignKey('self', related_name='children', blank=True, null=True)
    type = models.CharField(max_length=20, blank=True, default='', verbose_name=_(u'Тип страницы'))
    # type = models.CharField(max_length=20, blank=True, choices=PAGE_TYPES, db_index=True,
    # default=PAGE_TYPES[0][0], verbose_name=_(u'Тип страницы'))
    menu = models.CharField(max_length=20, blank=True, choices=MENU_TYPES, db_index=True,
                            default=MENU_TYPES[0][0], verbose_name=_(u'Тип меню'))
    order = models.PositiveSmallIntegerField(blank=True, default=0, verbose_name=_(u'Порядок'))
    is_active = models.BooleanField(blank=True, default=True, verbose_name=_(u'Активна'))

    def export(self):
        return {
            'id': self.id,
            'title': self.title,
            'type': self.type
        }

    @classmethod
    def export_all(cls, **kwargs):
        return [exhibitor.export() for exhibitor in cls.active.filter(**kwargs)]

    def __unicode__(self):
        return u'Страница ' + self.unicode_wrapper('title', default='Unnamed')

    class Meta:
        ordering = ('order',)
        verbose_name = _(u'страница')
        verbose_name_plural = _(u'Страницы')


# новости на главной странице
class NewsIndexTranslation(MultilingualTranslation):
    parent = models.ForeignKey('NewsIndex', related_name='translations')
    title = models.CharField(max_length=50, blank=True, default='', verbose_name=_(u'Заголовок'))
    # subtitle = models.CharField(max_length=50, blank=True, default='', verbose_name=_(u'Подзаголовок страницы'))
    body = models.TextField(blank=True, default='', verbose_name=_(u'Текст новости'))

    class Meta:
        unique_together = ('parent', 'language_code')


class NewsIndex(MultilingualModel, Base):
    resolutions_img = {
        'tn': {'w': 398, 'h': 297, 'crop': 'center', 'qual': 85, 'out_format': 'jpg'},
        'main': {'w': 550, 'h': 375, 'crop': 'center', 'qual': 93, 'out_format': 'jpg'},
    }

    newsTitle = models.CharField(max_length=20, blank=True, default='', verbose_name=_(u'Заголовок'))
    newsDate = models.DateField(default=datetime.today, blank=True, null=True, verbose_name=_(u'Дата новости'))
    newsText = models.TextField(blank=True, default='', verbose_name=_(u'Текст новости'))
    # newsorder = models.PositiveSmallIntegerField(blank=True, default=0, verbose_name=_(u'Порядок'))
    is_main = models.BooleanField(blank=True, default=False, verbose_name=_(u'Главная новость'))
    img = CustomImgField(file_path='', resolutions=resolutions_img, blank=True, null=True,
                         verbose_name=_(u'Изображение (550x375)'))

    def export(self):
        return {
            'id': self.id,
            'newsTitle': self.newsTitle,
            'newsDate': sels.newsDate,
            'newsText': self.newsText,
            'img': {'url': self.img.tn('tn') if self.img else ''},
        }

    @classmethod
    def import_item(cls, data, parent=None):
        img_data = data.pop('img')

        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        model.import_image(img_data)
        save_translations(temp_data, model, model.translations.model)
        model.subsection = parent
        model.save()

        return model

    def __unicode__(self):
        return self.unicode_wrapper('newsTitle', default='Unnamed')

    class Meta:
        verbose_name = _(u'новость')
        verbose_name_plural = _(u'Новости на главной')


# крупнейшие проекты на главной
class ProjectsIndexTranslation(MultilingualTranslation):
    parent = models.ForeignKey('ProjectsIndex', related_name='translations')
    projectTitle = models.CharField(max_length=200, blank=True, default='', verbose_name=_(u'Заголовок'))
    projectText = models.TextField(blank=True, default='', verbose_name=_(u'Подпись к проекту'))
    projectUrl = models.URLField(blank=True, default='', verbose_name=_(u'Ссылка'))

    class Meta:
        unique_together = ('parent', 'language_code')


class ProjectsIndex(MultilingualModel, Base):
    resolutions_img = {
        'tn': {'w': 398, 'h': 297, 'crop': 'center', 'qual': 85, 'out_format': 'jpg'},
        'main': {'w': 550, 'h': 375, 'crop': 'center', 'qual': 93, 'out_format': 'jpg'},
    }

    # projectTitle = models.CharField(max_length=50, blank=True, default='', verbose_name=_(u'Заголовок'))
    # projectText = models.TextField(blank=True, default='', verbose_name=_(u'Подпись к проекту'))
    # projectUrl = models.URLField(blank=True, default='', verbose_name=_(u'Ссылка'))
    img = CustomImgField(file_path='media/projects-index', resolutions=resolutions_img, blank=True, null=True,
                         verbose_name=_(u'Изображение (550x375)'))

    def export(self):
        return {
            'id': self.id,
            'projectTitle': self.projectTitle,
            'projectText': self.projectText,
            'img': {'url': self.img.tn('tn') if self.img else ''},
        }

    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    @classmethod
    def import_item(cls, data, parent=None):
        img_data = data.pop('img')

        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        model.import_image(img_data)
        save_translations(temp_data, model, model.translations.model)
        model.subsection = parent
        model.save()

        return model

    def __unicode__(self):
        return self.unicode_wrapper('projectTitle', default='Unnamed')

    class Meta:
        verbose_name = _(u'проект')
        verbose_name_plural = _(u'Проекты на главной')


# проекты в отдельных страницах
class ProjectsTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Projects', related_name='translations')
    projectTitle = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Заголовок'))
    projectText1 = models.TextField(blank=True, default='', verbose_name=_(u'Первый абзац'))
    projectText2 = models.TextField(blank=True, default='', verbose_name=_(u'Второй абзац'))
    projectText3 = models.TextField(blank=True, default='', verbose_name=_(u'Третий абзац'))
    projectListTitle = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Заголовок списка'))
    projectList = models.TextField(blank=True, default='', verbose_name=_(u'Список'))

    projectUrl = models.URLField(blank=True, default='', verbose_name=_(u'Ссылка'))

    class Meta:
        unique_together = ('parent', 'language_code')


class Projects(MultilingualModel, Base):
    resolutions_img = {
        'tn': {'w': 398, 'h': 297, 'crop': 'center', 'qual': 85, 'out_format': 'jpg'},
        'main': {'w': 550, 'h': 375, 'crop': 'center', 'qual': 93, 'out_format': 'jpg'},
    }
    img = CustomImgField(file_path='media/projects-index', resolutions=resolutions_img, blank=True, null=True,
                         verbose_name=_(u'Изображение (550x375)'))

    def export(self):
        return {
            'id': self.id,
            'projectTitle': self.projectTitle,
            'projectText1': self.projectText1,
            'projectText2': self.projectText2,
            'projectText3': self.projectText3,
            'projectListTitle': self.projectListTitle,
            'projectList': self.projectList,
            'img': {'url': self.img.tn('tn') if self.img else ''},
        }

    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    @classmethod
    def import_item(cls, data, parent=None):
        img_data = data.pop('img')

        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        model.import_image(img_data)
        save_translations(temp_data, model, model.translations.model)
        model.subsection = parent
        model.save()

        return model

    def __unicode__(self):
        return self.unicode_wrapper('projectTitle', default='Unnamed')

    class Meta:
        verbose_name = _(u'проект')
        verbose_name_plural = _(u'Проекты')


# направления
class DirectionsTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Directions', related_name='translations')
    fullName = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Полное название'))
    shortName = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Короткое название'))
    is_active = models.BooleanField(blank=True, default=True, verbose_name=_(u'Отображать направление на сайте'))
    desc = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Описание'))
    title = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Заголовок'))
    subText = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Дополнительный текст'))

    class Meta:
        unique_together = ('parent', 'language_code')


class Directions(MultilingualModel, Base):
    def export(self):
        return {
            'id': self.id,
            'fullName': self.fullName,
            'shortName': self.shortName,
            'is_active': self.is_active,
            'desc': self.desc,
            'title': self.title,
            'subText': self.subText,
        }

    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    def import_item(cls, data, parent=None):
        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        return model

    def export_front(self):
        return {
            'id': self.id,
            'fullName': self.fullName,
            'shortName': self.shortName,
            'is_active': self.is_active,
            'desc': self.desc,
            'title': self.title,
            'subText': self.subText,
        }

    def export_control(self):
        return {
            'id': self.id,
            'fullName': self.fullName,
            'shortName': self.shortName,
            'is_active': self.is_active,
            'desc': self.desc,
            'title': self.title,
            'subText': self.subText,
        }

    def __unicode__(self):
        return self.shortName

    class Meta:
        verbose_name = _(u'направление')
        verbose_name_plural = _(u'Направления')


# баннеры
class BannersTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Banners', related_name='translations')
    title = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Заголовок'))
    subTitle = models.TextField(blank=True, default='', verbose_name=_(u'Первый абзац'))
    url = models.URLField(blank=True, default='', verbose_name=_(u'Ссылка'))

    class Meta:
        unique_together = ('parent', 'language_code')


class Banners(MultilingualModel, Base):
    resolutions_img = {
        'tn': {'w': 398, 'h': 297, 'crop': 'center', 'qual': 85, 'out_format': 'jpg'},
        'main': {'w': 550, 'h': 375, 'crop': 'center', 'qual': 93, 'out_format': 'jpg'},
    }
    img = CustomImgField(file_path='media/projects-index', resolutions=resolutions_img, blank=True, null=True,
                         verbose_name=_(u'Изображение (550x375)'))
    color = models.BooleanField(blank=True, default=False, verbose_name=_(u'White text'))

    # def export(self):
    #     return {
    #         'id': self.id,
    #         'title': self.title,
    #         'subTitle': self.subTitle,
    #         'url': self.url,
    #         'color': self.color,
    #         'img': {'url': self.img.tn('tn') if self.img else ''},
    #     }

    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    def import_item(cls, data, parent=None):
        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        return model

    def export_front(self):
        return {
            'id': self.id,
            'title': self.title,
            'subTitle': self.subTitle,
            'url': self.url,
            'img': self.img,
            'color': self.color,
        }

    def export_control(self):
        return {
            'id': self.id,
            'title': self.title,
            'subTitle': self.subTitle,
            'url': self.url,
            'img': self.img,
            'color': self.color,
        }

    @classmethod
    def import_item(cls, data, parent=None):
        img_data = data.pop('img')

        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        model.import_image(img_data)
        save_translations(temp_data, model, model.translations.model)
        model.subsection = parent
        model.save()

        return model

    def __unicode__(self):
        return self.unicode_wrapper('title', default='Unnamed')

    class Meta:
        verbose_name = _(u'баннер')
        verbose_name_plural = _(u'Баннеры на главной')


class Partners(Base):
    resolutions_img = {
        'tn': {'w': 398, 'h': 297, 'crop': 'center', 'qual': 85, 'out_format': 'jpg'},
        'main': {'w': 550, 'h': 375, 'crop': 'center', 'qual': 93, 'out_format': 'jpg'},
    }
    title = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Компания'))
    img = CustomImgField(file_path='media/projects-index', resolutions=resolutions_img, blank=True, null=True,
                         verbose_name=_(u'Изображение (550x375)'))

    def export(self):
        return {
            'id': self.id,
            'title': self.title,
            'img': {'url': self.img.tn('tn') if self.img else ''},
        }

    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    @classmethod
    def import_item(cls, data, parent=None):
        img_data = data.pop('img')

        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        model.import_image(img_data)
        save_translations(temp_data, model, model.translations.model)
        model.subsection = parent
        model.save()

        return model

    def __unicode__(self):
        return self.unicode_wrapper('title', default='Unnamed')

    class Meta:
        verbose_name = _(u'логотип партнера')
        verbose_name_plural = _(u'Логотипы партнера')


# офисы в контактах
class OfficeTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Office', related_name='translations')
    office = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Офис'))
    desc = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Подпись к офису в заголовке'))
    address = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Адрес'))
    phone = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Телефон'))
    fax = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Факс'))
    email = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Email'))

    class Meta:
        unique_together = ('parent', 'language_code')


class Office(MultilingualModel, Base):
    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    @classmethod
    def import_item(cls, data, parent=None):
        model, temp_data = cls.simple_import(data)
        if model is None:
            return

        save_translations(temp_data, model, model.translations.model)

        return model

    def export_front(self):
        return {
            'id': self.id,
            'office': self.office,
            'desc': self.desc,
            'address': self.address,
            'phone': self.phone,
            'fax': self.fax,
            'email': self.email,
        }

    def export_control(self):
        lang = translation.get_language().capitalize()

        return {
            'id': self.id,
            'office' + lang: self.office,
            'desc' + lang: self.desc,
            'address' + lang: self.address,
            'phone' + lang: self.phone,
            'fax' + lang: self.fax,
            'email' + lang: self.email,
        }

    def __unicode__(self):
        return self.office

    class Meta:
        verbose_name = _(u'офис')
        verbose_name_plural = _(u'Офисы')


# отделы в контактах
class DeptsTranslation(MultilingualTranslation):
    parent = models.ForeignKey('Depts', related_name='translations')
    dept = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Отдел'))
    phone = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Телефон'))
    email = models.CharField(max_length=100, blank=True, default='', verbose_name=_(u'Email'))

    class Meta:
        unique_together = ('parent', 'language_code')


class Depts(MultilingualModel, Base):
    def export(self):
        return {
            'id': self.id,
            'dept': self.dept,
            'phone': self.phone,
            'email': self.email,
        }

    @classmethod
    def export_all(cls, **kwargs):
        return [projectindex.export() for projectindex in cls.objects.filter(**kwargs)]

    def __unicode__(self):
        return self.unicode_wrapper('dept', default='Unnamed')

    class Meta:
        verbose_name = _(u'отдел')
        verbose_name_plural = _(u'Отделы в контактах')
