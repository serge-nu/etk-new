# -*- coding: utf-8 -*-
import re

# noinspection PyUnusedLocal
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import FieldDoesNotExist
from django.http import HttpResponseRedirect
from django.utils import translation
from django.utils.translation import get_language

from config.settings import Lang
from config.settings import LANGUAGES_TUPLE

from django.conf import settings
from django.utils.translation import get_language

# from .models import MENU_TYPES, Page

#was
def debug(context):
    return {'DEBUG': settings.DEBUG}

# noinspection PyUnusedLocal
class MultilangMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):
        current_lang = get_language()
        new_lang = view_kwargs.pop('lang', None)
        if current_lang != Lang.Eng and not new_lang:
            translation.activate(Lang.Eng)
        elif new_lang and new_lang != current_lang:
            translation.activate(new_lang)


def my_login_required(function):
    def wrapped(request):
        if not request.user.is_authenticated():
            if get_language() == Lang.Eng:
                kwargs = {}
            else:
                kwargs = {'lang': get_language()}
            return HttpResponseRedirect(reverse('cosmoscow:vip', kwargs=kwargs))
        else:
            return function(request)

    return wrapped


FIRST_CAP_RE = re.compile('(.)([A-Z][a-z]+)')
ALL_CAP_RE = re.compile('([a-z0-9])([A-Z])')


# Конвертирует строку написанную в CamelCase (например MyVariable)
# в строку с подчеркивания (например my_variable)
def convert(name):
    s1 = FIRST_CAP_RE.sub(r'\1_\2', name)
    return ALL_CAP_RE.sub(r'\1_\2', s1).lower()


def create_or_update_and_get(model_class, data):
    instance = get_or_create(model_class, data)
    return update(instance, data)


def get_or_create(model_class, data):
    if model_class._meta.pk.name in data.keys():
        get_or_create_kwargs = {
            model_class._meta.pk.name: data.pop(model_class._meta.pk.name)
        }
    else:
        get_or_create_kwargs = {
            model_class._meta.pk.name: None
        }
    try:
        # get
        instance = model_class.objects.get(**get_or_create_kwargs)
    except model_class.DoesNotExist:
        # create
        instance = model_class(**get_or_create_kwargs)

    return instance


def update(instance, data):
    # update (or finish creating)
    for key, value in data.items():
        try:
            field = type(instance)._meta.get_field(key)
        except FieldDoesNotExist:
            continue
        if not field:
            continue
        if isinstance(field, models.ForeignKey) and hasattr(value, 'items'):
            rel_instance = get_or_create(field.rel.to, value)
            rel_instance = update(rel_instance, value)
            setattr(instance, key, rel_instance)
        elif isinstance(field, models.BooleanField):
            if value is None:
                value = field.get_default()
            setattr(instance, key, value)
        else:
            if not value and field.blank:
                value = field.get_default()
            setattr(instance, key, value)
    instance.save()
    return instance


def save_translations(data, instance, translation_model):
    for lang in LANGUAGES_TUPLE:
        translation_data = {}

        try:
            translation_instance = instance.translations.get(language_code=lang)
        except translation_model.DoesNotExist:
            translation_instance = translation_model(language_code=lang)
            translation_instance.parent = instance
            translation_instance.save()

        for key, value in data.iteritems():
            if key.endswith('_{lang}'.format(lang=lang)):
                translation_data[key[0:-3]] = value

        translation_instance = update(translation_instance, translation_data)


def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None


def find(f, seq):
    """Return first item in sequence where f(item) == True."""
    for item in seq:
        if f(item):
            return item


def first(query):
    try:
        return query.all()[0]
    except:
        return None


def tuplify(x):
    return x, str(x)


def date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))

# context
# def my_vars(request):
#     if get_language() == Lang.Eng:
#         lang_url = ''
#     else:
#         lang_url = get_language()
#
#     return {'DEBUG': settings.DEBUG,
#             'LANG_URL': lang_url,
#             'HEADER_MENU': Page.objects.filter(menu=MENU_TYPES[1][0], is_active=True),
#             'FOOTER_MENU': Page.objects.filter(menu=MENU_TYPES[2][0], is_active=True), }
