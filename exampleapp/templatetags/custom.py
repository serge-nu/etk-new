import re
import json

from django import template
from django.core.serializers import serialize
from django.core.urlresolvers import reverse, resolve, NoReverseMatch
from django.db.models.query import QuerySet
from django.template.loaders.filesystem import Loader
from django.utils.translation import get_language

register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, page, current_page=None, slug=None):
    """
    Template tag to highlight selected page in the menu
    """
    kwargs = {}
    lang = get_language()
    if lang == 'ru':
        kwargs['lang'] = lang
    if slug:
        kwargs['slug'] = slug
    try:
        page_url = reverse('etk:' + page.type, kwargs=kwargs)
        path = context.get('request').path
        if page_url == path:
            return 'active'
        elif current_page and current_page.parent == page:
            return 'active'
    except NoReverseMatch:
        pass
    return '123'


@register.simple_tag(takes_context=True)
def href(context, page, current_page=None, slug=None):
    """
    Template tag to transform page type to it's url
    """
    kwargs = {}
    lang = get_language()
    if lang == 'ru':
        kwargs['lang'] = lang
    if slug:
        kwargs['slug'] = slug
    try:
        page_url = reverse('cosmoscow:' + page.type, kwargs=kwargs)
        path = context.get('request').path
        if page_url == path:
            return ''
        elif current_page and current_page.parent == page:
            return ''
        else:
            return 'href="{0}"'.format(page_url)
    except NoReverseMatch:
        return ''


@register.simple_tag(takes_context=True)
def lang_url(context, lang=None):
    """
    Template tag to generate url for current page in another language
    """

    view = resolve(context.get('request').path)
    kwargs = {}
    if lang and lang != 'en':
        kwargs['lang'] = lang
    if context.get('selected_section') and context.get('photo_section'):
        kwargs['slug'] = context.get('selected_section').slug

    return reverse(view.view_name, kwargs=kwargs)


@register.filter()
def jsonify(obj):
    if isinstance(obj, QuerySet):
        return serialize('json', obj)
    return json.dumps(obj)


@register.simple_tag
def raw(template):
    source_loader = Loader()
    source, file_path = source_loader.load_template_source(template)

    try:
        source = re.sub(r'\|date:\"([^\"]*)\"', r'|date("\1")', source)
    except:
        pass

    try:
        source = re.sub(r'\|default:\"([^\"]*)\"', r'|default("\1")', source)
    except:
        pass

    # Convert 'include' tags to swig format

    grps = re.findall(
        r'({%\s*include\s+(?P<quote>[\"\'])(?P<filename>[^\"\']+)(?P=quote)(?:\s+with\s+(?P<params>[^%]*))?\s*%})',
        source, re.UNICODE | re.MULTILINE | re.IGNORECASE)

    if len(grps) > 0:

        if len(grps) > 1:
            grps2 = re.findall(r'(?P<name>[^\s=]+)=(?P<value>[^\s]*)', grps[1][3],
                               re.UNICODE | re.MULTILINE | re.IGNORECASE)
            parstring = ""
            parResults = []

            for param in grps2:
                parVal = re.sub(r'forloop\.counter(?P<startzero>0?)', r'loop.index\1', param[1])
                parResults.append(u'{% set ' + param[0] + ' = ' + parVal + ' %}')

            parString = u"".join(parResults)

            source = source.replace(grps[1][0], parString + u'{% include "' + grps[1][2] + '"' + ' %}')

    try:
        source = re.sub(r'\|safe', r'|raw', source)
    except:
        pass

    return source


class VerbatimNode(template.Node):
    def __init__(self, text):
        self.text = text

    def render(self, context):
        return self.text


@register.tag
def verbatim(parser, token):
    text = []
    while 1:
        token = parser.tokens.pop(0)
        if token.contents == 'endverbatim':
            break
        if token.token_type == template.TOKEN_VAR:
            text.append('{{')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('{%')
        text.append(token.contents)
        if token.token_type == template.TOKEN_VAR:
            text.append('}}')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('%}')
    return VerbatimNode(''.join(text))
