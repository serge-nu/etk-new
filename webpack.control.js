var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var autoprefixer = require('autoprefixer-core');
var csswring = require('csswring');


module.exports = {
    cache: true,
    entry: {
        'app': './assets/js/control/App'
    },
    output: {
        path: path.join(__dirname, 'static'),
        publicPath: 'static/',
        filename: '[name].js',
        chunkFilename: '[chunkhash].js'
    },
    resolve: {
        root: [path.join(__dirname, 'node_modules')],
        modulesDirectories: ['assets/js/control/']
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery'
        }),
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('bower.json', ['main'])
        ),
        new ExtractTextPlugin('style.css', {
            allChunks: true
        })
    ],
    module: {
        loaders: [
            {test: /\.jade$/, loader: 'jade'},
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('css-loader!postcss-loader!less-loader')
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('css-loader!postcss-loader')
            }
        ]
    },
    postcss: [autoprefixer({browsers: ['last 2 version', 'ie 9']}), csswring],
    externals: {
        jquery: 'jQuery'
    }
};
