var Backbone = require('backbone/backbone.js');

module.exports = Backbone.View.extend({
    tagName: 'li',

    itemName: 'этот объект',

    events: {
        'click .remove': 'removeItem'
    },

    initialize: function (options) {
        this.options = options || {};
        if (this.options.itemName) {
            this.itemName = this.options.itemName;
        }
    },

    render: function () {
        this.$el.html(this.template({
            model: this.model.toJSON(),
            cid: this.model.cid,
            lang: this.lang
        }));

        return this;
    },

    removeItem: function (e) {
        e.preventDefault();
        this.$el.remove();
        this.unbind();
        this.stopListening();

        if (!this.model.isNew()) {
            this.model.set('removed', true);
        } else {
            this.options.parent.collection.remove(this.model);
        }
        this.options.parent.updateSort();
    }
});