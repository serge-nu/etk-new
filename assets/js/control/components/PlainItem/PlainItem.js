var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');

var template = require('./PlainItem.jade');
var styles = require('./PlainItem.less');

var AbstractItemView = require('components/AbstractItem/AbstractItem');

module.exports = AbstractItemView.extend({
    tagName: 'li',

    className: 'ContactItem',

    events: {
        'change input': 'change',
        'click .remove': 'removeItem'
    },

    template: template,

    initialize: function (options) {
        this.options = options;
    },

    change: function (e) {
        var $input = $(e.target);
        this.model.set($input.attr('name'), $input.val());
    },

    render: function () {
        this.$el.html(this.template({
            model: this.model.toJSON(),
            cid: this.model.cid
        }));

        return this;
    },

    removeItem: function (e) {
        e.preventDefault();
        this.$el.remove();
        this.unbind();
        this.stopListening();

        if (!this.model.isNew()) {
            this.model.set('removed', true);
        } else {
            this.options.parent.collection.remove(this.model);
        }
        this.options.parent.updateSort();
    }
});
