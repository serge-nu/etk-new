var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');


module.exports = Backbone.View.extend({
    events: {
        'click .btn-save': 'save',
        'click .add-item': 'addItem'
    },

    initialize: function (options) {
        this.options = options || {};

        this._itemViews = [];

        this.$list = this.$('.list');
        this.$submit = this.$('.btn-save');

        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'add', this.renderElement);
    },

    render: function () {
        this.$list.children().not('.list-noremove').remove();
        this._itemViews = [];
        this.collection.each(function (model) {
            this.renderElement(model);
        }, this);
    },

    renderElement: function (model) {
        var view = new this.options.itemView(_.extend({
            model: model,
            parent: this
        }, {active: this.options.active}));

        if (this.options.prepend) {
            this.$list.prepend(view.render().el);
        } else {
            this.$list.append(view.render().el);
        }
        this._itemViews.push(view);
    },

    save: function (e) {
        e.preventDefault();
        var self = this;

        this.$submit.attr('disabled', 'disabled');
        this.$submit.spin('standard');

        this.collection.syncCollection(function () {
            self.$submit.removeAttr('disabled');
            self.$submit.spin(false);
            window.location.reload();
        });
    },

    addItem: function () {
        this.collection.add(new this.collection.model());
    }
});
