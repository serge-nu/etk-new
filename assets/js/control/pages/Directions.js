var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');

var spin = require('spin.js/spin.js');
var jquerySpin = require('spin.js/jquery.spin.js');

var AbstractListView = require('components/AbstractList/AbstractList');
var PlainItemView = require('components/PlainItem/PlainItem');
var ContactModel = require('models/Direction');
var ContactCollection = require('collections/Direction');

module.exports = Backbone.View.extend({
    el: 'body',

    events: {
        'click #save': 'save',
        'click #add-item': 'addItem'
    },

    initialize: function () {
        this.collection = new DirectionCollection(app.data.contacts);

        this.listView = new AbstractListView({
            itemView: PlainItemView,
            el: this.$('.list'),
            handle: '.icon-reorder',
            collection: this.collection
        });

        this.$submit = this.$('#save');
    },

    render: function () {
        this.listView.render();
    },

    addItem: function () {
        this.collection.add(new DirectionModel());
    },

    save: function (e) {
        e.preventDefault();
        var self = this;

        this.$submit.attr('disabled', 'disabled');
        this.$submit.spin('standard');

        this.listView.collection.syncCollection(function () {
            self.$submit.removeAttr('disabled');
            self.$submit.spin(false);
            window.location.reload();
        });
    }
});
