var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');

var spin = require('spin.js/spin.js');
var jquerySpin = require('spin.js/jquery.spin.js');

var AbstractListView = require('components/AbstractList/AbstractList');
var OfficeItemView = require('components/OfficeItem/OfficeItem');
var OfficeModel = require('models/Office');
var OfficeCollection = require('collections/Office');

module.exports = Backbone.View.extend({
    el: 'body',

    initialize: function () {
        this.officeCollection = new OfficeCollection(app.data.contacts);

        this.officeListView = new AbstractListView({
            itemView: OfficeItemView,
            el: this.$('.Office-section'),
            // handle: '.icon-reorder',
            collection: this.officeCollection
        });
    },

    render: function () {
        this.officeListView.render();
    }
});
