'use strict';

var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');

var ContactsView = require('pages/Contacts');
var DirectionsView = require('pages/Directions');

var Router = Backbone.Router.extend({
    routes: {
        'control/directions/': 'directions',
        'control/contacts/:lang/': 'contacts'
    },

    start: function () {
        var pushStateSupported = history && _.isFunction(history.pushState);
        Backbone.history.start({
            pushState: pushStateSupported
        });
    },

    activate: function (newView) {
        this.view = newView;
        this.view.render();
    },

    contacts: function () {
        this.activate(new ContactsView());
    }
});

var SingletonRouter = function () {
    if (window.router === undefined) {
        window.router = new Router();
    }
    return window.router;
};

module.exports = new SingletonRouter();
