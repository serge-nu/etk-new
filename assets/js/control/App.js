'use strict';

var router = require('router');
var settings = require('settings');

var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');

window.app = {
    initialize: function () {
        settings.configure();
        router.start();
    },

    vent: _.extend({}, Backbone.Events),
    data: {},
    models: {},
    collections: {},
    settings: settings.constants
};