var Backbone = require('backbone/backbone.js');

var CustomCollection = require('collections/CustomCollection');
var Office = require('models/Office');

module.exports = CustomCollection.extend({
    url: '/control/api/offices/',
    model: Office
});
