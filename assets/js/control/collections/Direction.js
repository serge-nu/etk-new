var Backbone = require('backbone/backbone.js');

var CustomCollection = require('collections/CustomCollection');
var Direction = require('models/Direction');

module.exports = CustomCollection.extend({
    url: '/control/api/directions/',
    model: Direction
});
