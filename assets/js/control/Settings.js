var _ = require('underscore/underscore.js');
var Backbone = require('backbone/backbone.js');

module.exports = {
    configure: function () {
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                function sameOrigin(url) {
                    // test that a given url is a same-origin URL
                    // url could be relative or scheme relative or absolute
                    var host = document.location.host, // host + port
                        protocol = document.location.protocol,
                        sr_origin = '//' + host,
                        origin = protocol + sr_origin;
                    // Allow absolute or scheme relative URLs to same origin
                    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                            // or any other URL that isn't scheme relative or absolute i.e relative.
                        !(/^(\/\/|http:|https:).*/.test(url));
                }

                if (sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", $('meta[name="csrf-token"]').attr('content'));
                }
            }
        });

        $.fn.spin.presets.standard = {
            left: '125%',
            lines: 10,
            length: 4,
            width: 2,
            radius: 3,
            color: '#000'
        };

        Backbone.Model.prototype.toJSON = function () {
            var json = _.clone(this.attributes);
            for (var attr in json) {
                if ((json[attr] instanceof Backbone.Model) || (json[attr] instanceof Backbone.Collection)) {
                    json[attr] = json[attr].toJSON();
                }
            }
            return json;
        };


    },

    constants: {
        staticUrl: '/static/',
        uploadUrl: '/control/api/upload_temp_file/',
        convertUrl: '/control/api/upload_and_convert/'
    }
};