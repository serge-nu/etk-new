$(document).ready(function(){

// инициализация слайдеров
    $('.Slider').owlCarousel({
        items:1,
        loop:true,
        nav: true,
        animateOut: 'fadeOut',
        autoplay:true,
        autoplayTimeout:5000,
        touchDrag  : false,
        mouseDrag  : false
    });

    $('.ProjectsSlider-container').owlCarousel({
        items:1,
        loop:true,
        nav: true,
        touchDrag  : false,
        mouseDrag  : false
    });

// выпадающее меню с годами на странице "новости"
    $('.NewsTimeLine-dropdown').click(function(){
        if ( $(this).hasClass('NewsTimeLine-dropdown--expanded') == false) {
            $(this).addClass('NewsTimeLine-dropdown--expanded');
            $(this).find('.NewsTimeLine-active').text(2015);
        } else {
            $(this).removeClass('NewsTimeLine-dropdown--expanded');
        }
    });

    $('.NewsTimeLine-year').click(function(){
        var newYear = $(this).text();
        $(this).siblings('.NewsTimeLine-active').text(newYear);
    });

    $(document).click(function(){
        $('.NewsTimeLine-dropdown').removeClass('NewsTimeLine-dropdown--expanded');

        $('.NewsTimeLine-dropdown').click(function(e){
                  e.stopPropagation();
        });
    });


// высота вертикального разделителя в новостях
    var newsHeight = $('.NewsPage').height();
    $('.VerticalDivider').height(newsHeight);

// переключение новостей
    $('.NewsTimeLine-item').click(function(){
        var order = $(this).attr('class').split(' ')[1].split('item')[1];
        $('.NewsPage-actual').removeClass('NewsPage-actual--active');
        $('.NewsPage').find('.NewsPage-actual' + order).addClass('NewsPage-actual--active');
        var actualHeight = $('.NewsPage-actual--active').height();
        $('.NewsTimeLine-list').height(actualHeight - 105);
        $('.VerticalDivider').height(actualHeight + 165);
    });

// смена подписей под слайдером на странице projects-inside
    // количество слайдов
    var slides = $('.ProjectsSlider-item').length / 2;
    $('.ProjectsInfo .owl-next').click(function(){
        // No. активного слайда
        var slideNumber = $('.ProjectsSlider-desc--active').attr('class').split('--')[1].split(' ')[0];
        var newSlide = +slideNumber + 1;
        // новый круг
        if (slideNumber == slides) newSlide = 1;
        // смена подписи
        $('.ProjectsSlider-desc--active').removeClass('ProjectsSlider-desc--active');
        $('.ProjectsSlider-desc--'+ newSlide).addClass('ProjectsSlider-desc--active');
    });

});
