var gulp = require('gulp');

var gulpJade = require('gulp-jade');

var postcss = require('gulp-postcss');
var less = require('gulp-less');
var autoprefixer = require('autoprefixer-core');
var csswring = require('csswring');

// new
var svgSprite = require('gulp-svg-sprite');
var svgo = require('gulp-svgo');
var connect = require('gulp-connect');

var webpack = require('webpack');
var gulpWebpack = require('gulp-webpack');
var webpackConfig = require('./webpack.control.js');

// CSS
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

// Utils
var rename = require('gulp-rename');

// new
var myConfig = Object.create(webpackConfig);
myConfig.watch = true;
myConfig.devtool = 'sourcemap';
myConfig.debug = true;

gulp.task('assets', function () {
    gulp.src('./assets/js/libs/**/*')
        .pipe(gulp.dest('static/js/libs/'));
});

gulp.task('js', function () {
    gulp.src('assets/js/controlApp.js')
        .pipe(gulpWebpack(myConfig))
        .pipe(gulp.dest('static/js/control/'));
});

gulp.task('default', ['js', 'assets']);

// CSS
// gulp.task('css', function () {
//     var processors = [
//         autoprefixer({browsers: ['last 2 version']}),
//         csswring
//     ];
//
//     return gulp.src('./assets/css/front/style.less')
//         .pipe(less())
//         .pipe(postcss(processors))
//         .pipe(gulp.dest('./static/css/front'));
// });

// CSS Control
gulp.task('css-control', function () {
    var processors = [
            autoprefixer({browsers: ['last 2 version']}),
            csswring
        ];

    return gulp.src('./assets/css/control/style.less')
        .pipe(less())
        // .pipe(postcss(processors))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(minifyCSS({
            noAdvanced: true,
            keepSpecialComments: 0
        }))
        .pipe(rename({
            basename: 'control',
            suffix: '.min'
        }))
        .pipe(gulp.dest('./static/css/control'));
});


// Копирование не изменяемых статических файлов
gulp.task('assets', function () {
    gulp.src('./assets/img/**/*')
        .pipe(gulp.dest('static/img'));
    gulp.src('./assets/fonts/**/*')
        .pipe(gulp.dest('static/fonts'));
});

// SVG
gulp.task('svg', function () {
    return gulp.src('assets/svg/*.svg')
        .pipe(svgo())
        .pipe(svgSprite({
            mode: {
                symbol: true
            }
        }))
        .pipe(gulp.dest('static/svg'));
});

// Шаблоны
// gulp.task('templates', function () {
//     gulp.src('./assets/templates/**/*.*')
//         .pipe(gulpJade({pretty: true}))
//         .pipe(gulp.dest('./templates/front'));
// });

gulp.task('watch', function () {
    gulp.watch(['./assets/css/**/*'], ['css-control']);
    gulp.watch(['./assets/img/**/*'], ['assets']);
    gulp.watch(['./assets/fonts/**/*'], ['assets']);
    gulp.watch(['./assets/js/control/**/*'], ['js']);
    gulp.watch(['./assets/svg/**/*'], ['svg']);
    // gulp.watch(['./assets/templates/index.jade'], ['templates']);
});

// gulp.task('webserver', function () {
//     connect.server({
//         port: 9000
//     });
// });

// gulp.task('default', ['css', 'assets', 'svg', 'templates', 'watch', 'webserver']);
gulp.task('default', ['css-control', 'assets', 'svg', 'js', 'watch']);
